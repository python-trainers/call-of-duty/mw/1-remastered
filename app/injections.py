from trainerbase.codeinjection import CodeInjection, AllocatingCodeInjection
from memory import h1_sp64_ship, player_coords_pointer


infinite_ammo = CodeInjection(h1_sp64_ship.exe + 0x4AF658, "nop\n" * 4)

update_player_coords_pointer = AllocatingCodeInjection(
    h1_sp64_ship.exe + 0x4C696D,
    f"""
        pop rax

        push rbx
        mov rbx, {player_coords_pointer}
        mov [rbx], rax
        pop rbx

        movss xmm0, [rax + 0x80]
        movss xmm1, [rax + 0x84]

        push rax
    """,
    original_code_length=16,
    is_long_x64_jump_needed=True,
)
