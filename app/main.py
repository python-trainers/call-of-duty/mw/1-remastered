from os import _exit as force_exit

from pymem.exception import MemoryWriteError

from scripts import system_script_engine
from gui import run_menu
from injections import update_player_coords_pointer


def on_initialized():
    update_player_coords_pointer.inject()
    system_script_engine.start()


def on_shutdown():
    system_script_engine.stop()

    try:
        update_player_coords_pointer.eject()
    except MemoryWriteError:
        pass


def main():
    run_menu(on_initialized=on_initialized)
    on_shutdown()
    force_exit(0)


if __name__ == "__main__":
    main()
