from trainerbase.common import Teleport, Vector3
from objects import player_x, player_y, player_z


class MWRemasteredTeleport(Teleport):
    def dash(self):
        super().dash()
        self.player_z.value += 10


tp = MWRemasteredTeleport(
    player_x,
    player_y,
    player_z,
    minimal_movement_vector_length=10.0,
    dash_coefficients=Vector3(170, 170, 170),
)
