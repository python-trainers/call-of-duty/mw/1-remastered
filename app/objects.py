from trainerbase.gameobject import GameInt, GameFloat
from trainerbase.memory import Address

from memory import player_base_pointer, player_coords_pointer


player_base_address = Address(player_base_pointer, [0x0])
player_coords_address = Address(player_coords_pointer, [0x80])

hp = GameInt(player_base_address.inherit(new_add=0x184))

player_x = GameFloat(player_coords_address.inherit(new_add=0x0))
player_y = GameFloat(player_coords_address.inherit(new_add=0x4))
player_z = GameFloat(player_coords_address.inherit(new_add=0x8))
