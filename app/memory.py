from types import SimpleNamespace
from trainerbase.memory import POINTER_SIZE, pm


h1_sp64_ship = SimpleNamespace(exe=pm.base_address)

player_base_pointer = h1_sp64_ship.exe + 0x56DBA50
player_coords_pointer = pm.allocate(POINTER_SIZE)
