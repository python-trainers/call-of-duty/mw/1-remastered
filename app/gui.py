import dearpygui.dearpygui as dpg
from trainerbase.gui import (
    add_teleport_to_gui,
    add_gameobject_to_gui,
    add_codeinjection_to_gui,
    simple_trainerbase_menu,
)

from objects import hp
from injections import infinite_ammo
from teleport import tp


@simple_trainerbase_menu("Modern Warfare® Remastered", 620, 350)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main", tag="code_injections"):
            add_codeinjection_to_gui(infinite_ammo, "Infinite Ammo", "F1")
            add_gameobject_to_gui(hp, "HP", "F2")

        with dpg.tab(label="Teleport"):
            add_teleport_to_gui(tp, "Insert", "Home", "X")
